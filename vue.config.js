const path = require('path')

function resolve(dir) {
	return path.join(__dirname, dir)
}

module.exports = {
	chainWebpack: config => {
		config.resolve.alias
			.set('@', resolve('src'))
			.set('views', resolve('src/views'))
			.set('components', resolve('src/components'))
		config.when(
			process.env.NODE_ENV === 'development',
			config => config.devtool('source-map')
		)
	}
}
