import Home from 'views/Home.vue'
import Detail from 'views/Detail.vue'
import Plate from 'views/Plate.vue'
import Search from 'views/Search.vue'
import About from 'views/About.vue'

export default [
	{
		path: '/',
		redirect: () => '/home'
	},
	{
		name: 'Home',
		path: '/home',
		component: Home
	},
	{
		name: 'Plate',
		path: '/plate',
		component: Plate
	},
	{
		name: 'Detail',
		path: '/detail',
		component: Detail,
		beforeEnter: (to, from, next) => {
			window.document.title = '阿伟新闻网-' + to.params.text
			next()
		}
	},
	{
		name: 'Search',
		path: '/search',
		component: Search,
		beforeEnter: (to, from, next) => {
			window.document.title = '阿伟新闻网-' + to.params
			next()
		}
	},
	{
		name: 'About',
		path: '/about',
		component: About
	},
	{
		path: '*',
		redirect: () => '/'
	}
]
