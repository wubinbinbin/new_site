import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import state from './state'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
	state, // 数据存放
	getters, // 相当于计算属性
	actions, // 异步方法
	mutations, // 同步方法
	plugins: [createPersistedState()]
})
