export default {
	changeWord(state, payload) {
		state.word = payload
		state.history.splice(0, 0, {
			text: payload,
			id: new Date().getTime()
		})
		if (state.history.length > 10) {
			state.history.splice(-1)
		}
	},
	resetHistory(state) {
		state.history = []
	},
	saveNews(state, payload) {
		state.news.push(...payload)
	}
}
