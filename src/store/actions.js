import axios from 'axios'
import { neteasy } from './service.json'

export default {
	async getList({ commit, state }) {
		const page = state.news.length / 20 + 1
		const result = await axios
			.post(neteasy, {
				page: page,
				count: '20'
			})
			.then(res => {
				if (res.data.code === 200) {
					return res.data.result.map((item, index) => {
						item.id = 10000 * page + index
						return item
					})
				}
				return []
			})
		commit('saveNews', result)
	}
}
